package com.bamboorose.rest2

import android.os.Parcel
import android.os.Parcelable

data class User (
        val login: String = "",
        val id: Long = 0,
        val url: String = "",
        val repos_url: String = "",
        val followers_url: String = "",
        val type: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(login)
        parcel.writeLong(id)
        parcel.writeString(url)
        parcel.writeString(repos_url)
        parcel.writeString(followers_url)
        parcel.writeString(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }
}