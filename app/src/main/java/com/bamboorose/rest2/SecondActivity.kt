package com.bamboorose.rest2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val user: User = intent.getParcelableExtra("User")

        val id: TextView = findViewById(R.id.user_id)
        val username: TextView = findViewById(R.id.username)
        val url: TextView = findViewById(R.id.url)
        val repos: TextView = findViewById(R.id.repos_url)
        val followersUrl: TextView = findViewById(R.id.followers_url)
        val type: TextView = findViewById(R.id.type)

        id.text =(getString(R.string.userId_label, user.id))
        username.text =(getString(R.string.username_label, user.login))
        url.text =(getString(R.string.url_label, user.url))
        repos.text =(getString(R.string.repos_url_label, user.repos_url))
        followersUrl.text =(getString(R.string.followers_url_label, user.followers_url))
        type.text = getString(R.string.type_label, user.type)
    }
}
