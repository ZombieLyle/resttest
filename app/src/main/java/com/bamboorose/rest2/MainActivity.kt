package com.bamboorose.rest2

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    var users = listOf<User>()
    var userNames: MutableList<String> = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val listview: ListView = findViewById<ListView>(R.id.lv_users)


        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, userNames)

        val client: GithubAPI = GithubAPI.create()
        val call: Call<List<User>> = client.fetch()

        call.enqueue(object: Callback<List<User>> {
            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                users = response.body()!!
                for(user: User in users) {
                    userNames.add(user.login)
                }
                adapter.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<List<User>>?, t: Throwable?) {
                TODO("not implemented")
            }
        })


        listview.adapter = adapter

        listview.onItemClickListener = object : AdapterView.OnItemClickListener {

            override fun onItemClick(parent: AdapterView<*>, view: View,
                                     position: Int, id: Long) {

                val intent = Intent(this@MainActivity, SecondActivity::class.java)
                intent.putExtra("User", users.get(position))
                startActivity(intent)
            }
        }
    }
}