package com.bamboorose.rest2

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

interface GithubAPI {

    @retrofit2.http.GET("/users")
    fun fetch(): Call<List<User>>

    companion object {
        fun create(): GithubAPI {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://api.github.com/")
                    .build()
            return retrofit.create(GithubAPI::class.java)
        }
    }
}